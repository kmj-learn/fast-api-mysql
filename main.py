from random import randint, randrange
from fastapi import FastAPI
import mysql.connector

mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  password="Jancok123",
  database="fastapi"
)

app = FastAPI(
    title="My Little Api"
)


@app.get("/")
async def root():
    return {"message": "Hello World"}

@app.get("/products")
async def products():
    mycursor = mydb.cursor()
    mycursor.execute("SELECT * FROM product")
    data = []
    for row in mycursor.fetchall():
        data.append({"id":row[0], "code": row[1], "name": row[2]})

    return data

@app.post("/products/insert")
async def product_insert():
    mycursor = mydb.cursor()
    try:
        sql = "INSERT INTO product (code, name) VALUES (%s, %s)"
        code = randint(1, 10000)
        name = "Product %s"%(code)
        val = (code, name)
        mycursor.execute(sql, val)

        mydb.commit()
    except:
        return {"error": "Something else went wrong"}

    return {"ok"}

@app.get("/products/{id}")
async def show(id:int):
    mycursor = mydb.cursor()
    try:
        sql = "SELECT * FROM product WHERE id=%i"%(id)
        
        mycursor.execute(sql)

        myresult = mycursor.fetchone()
        mycursor.close()

        return {
            "id": myresult[0],
            "code": myresult[1],
            "name": myresult[2]
        }
    except:
        return {

        }

@app.delete("/products/{id}")
async def delete(id:int):
    mycursor = mydb.cursor()
    try:
        sql = "DELETE FROM product WHERE id=%i"%(id)
        
        mycursor.execute(sql)
        mycursor.commit()
        mycursor.close()

        return {
            "OK"
        }
    except:
        return {
            "something error occure."
        }